/*---------------------------------------------------------------
# Package - Joomla Template based on Helix Framework   
# ---------------------------------------------------------------
# Author - JoomShaper http://www.joomshaper.com
# Copyright (C) 2010 - 2012 JoomShaper.com. All Rights Reserved.
# license - PHP files are licensed under  GNU/GPL V2
# license - CSS  - JS - IMAGE files  are Copyrighted material 
# Websites: http://www.joomshaper.com
-----------------------------------------------------------------*/
window.addEvent('domready', function(){
	var c_height = document.id('header-wrapper').getHeight();
	var slide_show = document.id('sp-slide-grid');
	if (slide_show) slide_show.setStyle('margin-top', - c_height);
	sp_margin('header');
	sp_margin('sp-ticket', 'left');
})

window.addEvent('resize', function(){
	var c_height = document.id('header-wrapper').getHeight();
	var slide_show = document.id('sp-slide-grid');
	if (slide_show) slide_show.setStyle('margin-top', - c_height);
	sp_margin('header');
	sp_margin('sp-ticket', 'left');
})

function sp_margin(container, dir){
	if (typeof(dir)==='undefined') dir = 'right';
	var rtl = document.getElement('.rtl');
	if (rtl)  {
		if (dir==='right') {
			dir = 'left';
		} else {
			dir = 'right';
		}
	}
	var c_width	= (window.getSize().x - document.getElements('.sp-wrap')[0].getWidth())/2;
	document.id(container).setStyle('margin-' + dir, - c_width );
}