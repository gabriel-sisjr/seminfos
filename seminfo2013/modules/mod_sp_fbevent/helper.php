<?php
	/*
		# mod_sp_fbevent - Facebook event Module by JoomShaper.com
		# ------------------------------------------------------------------------
		# Author    JoomShaper http://www.joomshaper.com
		# Copyright (C) 2010 - 2012 JoomShaper.com. All Rights Reserved.
		# License - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
		# Websites: http://www.joomshaper.com
	*/

    // no direct access
    defined('_JEXEC') or die('Restricted access');

    class Mod_SP_FBEvent
    {	
        private $params;
        private $accessToken;
        private $moduleID;
		private $errors = array();
		
        //Initiate configurations
        public function __construct($params, $id)
        {
            $this->params = $params;
            $this->moduleID = $id;
            $this->accessToken = $this->getAccessToken();
            return $this;
        }
		
        public function error($index=null)
        {
            if( !empty($this->errors) )
            {
                if( is_null($index) ) return  implode("<br />",$this->errors) ; 
                else return  $this->errors[$index]; 
            } 
            else return false;
        }		

        /**
        * Get Access tocken
        * 
        * @return string
        */		
        public function accessToken()
        {
            return $this->accessToken;
        }

        /**
        * Generate access tocken
        * @access private
        * @return array
        */		
        private function getAccessToken()
        {
            $data = $this->Cache( 'access_token.txt', array($this,'getCurl'),
                array(
                    'https://graph.facebook.com/oauth/access_token',
                    array( 'client_id'=>trim($this->params->get('app_id')),
                        'client_secret'=>trim($this->params->get('app_secret')),
                        'grant_type'=>'client_credentials',  )
                ),

                (60*24*3), array($this,'onError') );

            parse_str($data, $output);
            return $output['access_token'];
        }

        /**
        * Event data
        * 
        * @return array
        */		
        public function getEventData()
        {
            $data = $this->Cache( 'event-data.json', array($this,'getCurl'),
                array(
                    'https://graph.facebook.com/'.$this->params->get('event_id'),
                    array( 'access_token'=>$this->accessToken)
                ),
                3600, array($this,'onError') );
            return json_decode($data,true); 
        }

        /**
        * Event feeds
        * 
        * @return array
        */		
        public function getEventFeed()
        {

            $data = $this->Cache( 'event-feed-data.json', array($this,'getCurl'),
                array(
                    'https://graph.facebook.com/'.$this->params->get('event_id').'/feed',
                    array( 'access_token'=>$this->accessToken, 'limit'=>$this->params->get('feed_limit'))
                ),

                900, array($this,'onError') );
            return json_decode($data,true); 
        }



        /**
        * number of attende
        *
        * @return array
        */
        public function getAttending()
        {
		
            $data = $this->Cache( 'total-attending.json', array($this,'getCurl'),
                array(
                    'https://graph.facebook.com/'.$this->params->get('event_id').'/attending',
                    array( 'access_token'=>$this->accessToken, 'limit'=>5000)
                ),
                900, array($this,'onError') );
            return json_decode($data,true); 
        }


        /**
        * Event date time parser
        * 
        * @param string $event
        * @param boolean $showutc   default is false
        * @return string
        */
        public function getDateTime($event, $showutc=false)
        {
            $esd = explode('T', $event);
            $date = @$esd[0];
            $timeandzone = @$esd[1];
            $est = explode('+',$timeandzone);
            $time = $est[0];
            $zone = @substr($est[1], 0, -2);
            if( $showutc ) return 'UTC+'.$zone;
            return $date.' '.$time;
        }

        /**
        * PHP CURL function
        * 
        * @param string $url
        * @param array $query   default is array
        * @return string
        */
        private function getCurl($url, $query=array()) {
            $requestURL =  $url;
            if( !empty($query) and is_array($query) ) $requestURL .= '?'. http_build_query($query,'','&');
            if (function_exists('curl_init'))
            {
                // initializing connection
                $curl = curl_init();
                // saves us before putting directly results of request
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                // url to get
                curl_setopt($curl, CURLOPT_URL, $requestURL );
                // timeout in seconds
                curl_setopt($curl, CURLOPT_TIMEOUT, 20);
                // useragent
                curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
                // reading location
                $data = curl_exec($curl);
                // closing connection
                $error = trim(curl_error($curl)); 
                curl_close($curl);
                if( !empty($error) ) $this->errors[] = '"'.$error.'" in module "'.basename(dirname(__FILE__)).'"';
                return $data;
            }
            else
            {
                $this->errors[] = 'cURL extension is not available on your server.  in module "'.basename(dirname(__FILE__)).'"';
            }
        }

        /**
        * Simple caching function
        * @version  1.3
        * @param string $file
        * @param string | array $datafn                  e.g:  functionname |  array( object, function) ,
        * @param array  $datafnarg    default is array  e.g:   array( arg1, arg2, ...) ,       
        * @param mixed $time       default is 900  = 15 min
        * @param mixed $onerror    string function or array(object, method )
        * @return string
        */

        private function Cache( $file,  $datafn, $datafnarg=array(), $time=900, $onerror='')
        {
            // check joomla cache dir writable
            $dir = basename(dirname(__FILE__));
            if (is_writable(JPATH_CACHE))
            {
                // check cache dir or create cache dir
                if (!file_exists(JPATH_CACHE.'/'.$dir)) mkdir(JPATH_CACHE.'/'.$dir.'/', 0755);
                $cache_file = JPATH_CACHE.'/'.$dir.'/'.$this->moduleID.'-'.$file;
                // check cache file, if not then write cache file
                if ( !file_exists($cache_file) )
                {
                    $data =  call_user_func_array($datafn, $datafnarg);
                    JFile::write($cache_file, $data);
                }  
                // if cache file expires, then write cache
                elseif ( filesize($cache_file) == 0 || ((filemtime($cache_file) + (int) $time ) < time()) )
                {
                    $data =  call_user_func_array($datafn, $datafnarg);
                    JFile::write($cache_file, $data);
                }
                // read cache file
                $data =  JFile::read($cache_file);
                $params['file'] = $cache_file;
                $params['data'] = $data;
                if( !empty($onerror) ) call_user_func($onerror, $params);
                return $data;
            } else {
                return   call_user_func_array($datafn, $datafnarg);
            }
        }

        public function onError($params)
        {
            $data = json_decode($params['data'],true);
            if( isset( $data['error'] ) ) JFile::Delete($params['file']);
        }
}
