<?php
	/*
		# mod_sp_fbevent - Facebook event Module by JoomShaper.com
		# ------------------------------------------------------------------------
		# Author    JoomShaper http://www.joomshaper.com
		# Copyright (C) 2010 - 2012 JoomShaper.com. All Rights Reserved.
		# License - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
		# Websites: http://www.joomshaper.com
	*/
    // no direct access
    defined( '_JEXEC' ) or die( 'Restricted access' );
	
?>
<div id="fb-root"></div>
<div id="sp-fb-event" class="sp-fb-event">
	<!--Start Event Info-->
	<div id="sp-fb-event-info">
		<h3><a target="_blank" href="https://www.facebook.com/events/<?php echo $event['id'] ?>/"><?php echo $event['name'] ?></a></h3>		
		<div class="sp-fb-event-time-place">
			<span class="sp-fb-event-time"><?php echo $params->get('event_time_prefix_text') ?>  
				<?php echo   date($params->get('event_time_format'), strtotime($helper->getDateTime($event['start_time']))); ?> 
				<?php if(isset($event['end_time'])){ ?>
					<?php echo $params->get('event_time_duration_seperator') ?> 
					<?php echo   date($params->get('event_time_format'), strtotime( $helper->getDateTime($event['end_time']))) ?> 
					<?php } ?>
				<?php echo $params->get('event_time_suffix_text') ?>
				<?php echo $helper->getDateTime($event['start_time'],true); ?>
			</span>
			
			<span class="sp-fb-event-place">
				<?php echo $params->get('event_place_prefix_text') ?> 
				<?php echo $event['location'] ?>
				<?php echo $params->get('event_place_suffix_text') ?>
			</span>
		</div>
		<div style="clear: both;"></div>
	</div>
	<!--End Event Info-->

	<!--End Event Description-->
	<div class="sp-fb-event-desc-wrapper">
		<?php if($params->get('show_event_creator')=='1'){ ?>
			<span class="sp-fb-event-host"><?php echo $params->get('event_creator_prefix_text') ?>
				<a target="_blank" href="https://www.facebook.com/profile.php?id=<?php echo $event['owner']['id'] ?>">
				<?php echo $event['owner']['name'] ?></a><?php echo $params->get('event_creator_suffix_text') ?>
			</span>
		<?php } ?>

		<?php if($params->get('show_attended_count')=='1'): ?>
			<span class="sp-fb-event-going"><?php 
				echo count($attending['data']).' ';
				echo (count($attending['data'])>1)? JText::_('PEOPLE_ARE_GOING') :  JText::_('PEOPLE_IS_GOING');?>
			</span>
		<?php endif; ?>
		
		<div style="clear:both"></div>
	
		<?php if($params->get('show_event_logo')=='1'){ ?>
			<a target="_blank" href="https://www.facebook.com/events/<?php echo $event['id'] ?>/"><img class="sp-fb-event-logo" src="https://graph.facebook.com/<?php echo $event['id'] ?>/picture" alt="<?php echo $event['name'] ?>" title="<?php echo $event['name'] ?>" /></a>
		<?php } ?>
		<div class="sp-fb-event-desc">
			<?php echo $event['description'] ?>
		</div>
	</div>
	<!--End Event Description-->
	

	<?php if($params->get('show_comments')=='1') { ?>
		<div class="sp-fb-event-feed">
			<ul>
				<?php foreach( (array) $feed['data'] as $ind=>$val)
					{ ?>
					<li> 
						<!--Commentor Avatar-->
						<a target="_blank" href="https://www.facebook.com/profile.php?id=<?php echo $val['from']['id'] ?>">
							<img src="https://graph.facebook.com/<?php echo $val['from']['id'] ?>/picture" alt="<?php echo $val['from']['name'] ?>" title="<?php echo $val['from']['name'] ?>" />
						</a>
						<!--Commentor Avatar-->
						
						<div class="sp-fb-event-feed-text">
							<a target="_blank" href="https://www.facebook.com/profile.php?id=<?php echo $val['from']['id'] ?>" class="sp-fb-comment-user"><?php echo $val['from']['name'] ?></a>
							
							<p class="sp-fb-event-feed-msg"><?php echo $val['message'] ?></p>
							
							<?php if(isset($val['likes']['count']) and  $val['likes']['count']>0 and $params->get('show_comment_like')=='1'){ ?>
								<span class="fb-like"><?php echo $val['likes']['count'] ?> <?php echo ($val['likes']['count']>1)?JText::_('SP_FB_EVENT_PEOPLE_LIKE'):JText::_('SP_FB_EVENT_PEOPLE_LIKES') ?></span>
								<?php } 

								if($val['comments']['count']>0 and $params->get('hide_child_comments')=='0')
								{

								?>
								<div style="clear:both"></div>
								<ul>
									<?php foreach($val['comments']['data'] as $i=>$v){ ?>
										<li><a target="_blank" href="https://www.facebook.com/profile.php?id=<?php echo $v['from']['id'] ?>" class="sp-fb-comment-user"><?php echo $v['from']['name'] ?></a><br /><?php echo $v['message'] ?>
											<div style="clear:both"></div>
											<?php if(isset($v['likes'])   and $params->get('show_comment_like')=='1' )
												{ ?>
												<span class="fb-like"> <?php echo $v['likes'] ?> <?php echo ($v['likes']>1)?JText::_('SP_FB_EVENT_PEOPLE_LIKE'):JText::_('SP_FB_EVENT_PEOPLE_LIKES') ?></span>
												<?php } ?>
										</li>
										<?php } ?>
								</ul>
								<?php  } ?>
								<div style="clear:both"></div>	
						</div>
					</li>
					<?php
					}
				?>
			</ul>
		</div>
	<?php } ?>
</div>
