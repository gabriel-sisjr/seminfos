<?php
	/*
		# mod_sp_fbevent - Facebook event Module by JoomShaper.com
		# ------------------------------------------------------------------------
		# Author    JoomShaper http://www.joomshaper.com
		# Copyright (C) 2010 - 2012 JoomShaper.com. All Rights Reserved.
		# License - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
		# Websites: http://www.joomshaper.com
	*/

    // no direct access
    defined('_JEXEC') or die('Restricted access');
    //Parameters
    $ID 				= $module->id;
    $style				= $params->get('style','default');
    $moduleName         = basename(dirname(__FILE__));

    // Include helper.php
    require_once (dirname(__FILE__).'/helper.php');
    $document 			= JFactory::getDocument();
    $helper 			= new Mod_SP_FBEvent($params, $ID);
    $event				= $helper->getEventData();
    if($params->get('show_comments')=='1') $feed = $helper->getEventFeed();
    if($params->get('show_attended_count')=='1')  $attending = $helper->getAttending();

    if (is_array($event)) {
        $cssFile = JPATH_THEMES. '/'.$document->template.'/css/'.$moduleName.'.css';
        if(file_exists($cssFile))
        {
            $document->addStylesheet(JURI::base(true) . '/templates/'.$document->template.'/css/'. $moduleName . '.css');
        } else {
            $document->addStylesheet(JURI::base(true) . '/modules/'.$moduleName.'/assets/css/' . $moduleName . '.css');
        }
        require(JModuleHelper::getLayoutPath($moduleName)); 
    } else {
         if( $helper->error(0) ) JFactory::getApplication()->enqueueMessage( $helper->error(0) , 'error');
    }