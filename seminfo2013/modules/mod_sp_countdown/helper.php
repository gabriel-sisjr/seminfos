<?php
    /*------------------------------------------------------------------------
	# mod_sp_stock - Yahoo stock module by JoomShaper.com
	# ------------------------------------------------------------------------
	# author    JoomShaper http://www.joomshaper.com
	# Copyright (C) 2010 - 2012 JoomShaper.com. All Rights Reserved.
	# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	# Websites: http://www.joomshaper.com
    -------------------------------------------------------------------------*/

    // no direct access
    defined('_JEXEC') or die('Restricted access');

    /**
    * 
    */
  

    class Mod_SP_Countdown
    {	
        private $config;
        private $raw_data;
        private $raw_url;
        private $base_url = 'http://chart.finance.yahoo.com/z?';
        private $symbols = array();

        //Initiate configurations
        public function __construct($params) {
            $this->raw_url = $this->generateURL($params);
            array_push($this->symbols,'"'.$params->get('stock_id').'"');

            if( $params->get('comparing_ids')!='' ) {
                $z = explode(',',$params->get('comparing_ids'));
                foreach($z as $v) array_push($this->symbols,'"'.trim($v).'"');
            }

            return $this;
        }
    }
