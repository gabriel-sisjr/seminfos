<?php
/**
 * @version		$Id: vertical.php 10136 2011-12-07 11:09:45Z hieudm $
 * @package		Joomla.Site
 * @subpackage	mod_articles_news
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="jsn-mod-newsflash jsn-vertical-container">
<?php for ($i = 0, $n = count($list); $i < $n; $i ++) : ?>
	<div class="jsn-article-container">
	<?php $item = $list[$i]; ?>
	<?php require JModuleHelper::getLayoutPath('mod_articles_news', '_item');
	if ($n > 1 && (($i < $n - 1) || $params->get('showLastSeparator'))) : ?>
		<span class="article-separator">&#160;</span>
	<?php endif; ?>
	</div>
<?php endfor; ?>
</div>