<?php $slidedisable	= $this->params->get("slidedisable");     ?>	 
	
<div id="slideshow">    
 <div class="pagewidth">        
 <div id="slider">		   
 <div class="inner">           
 <div id="phone-apple" class="panel">                
 <div class="text">                                      
 <h1>I phone Apple 4s</h1>                     
 <h2>for your corporate</h2>                
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem 		
 ipsum dolor sit amet, consectetur adipiscing elit. 				
 Donec blandit enim libero, quis tincidunt arcu.</p>           
 </div>                                           
 <img src="templates/<?php echo $this->template ?>/images/slide1.png" alt="Bolder Theme, for your corporate site" />            
 </div>                                

 <div id="pad-apple" class="panel none">                  
 <div class="text">                                    
 <h1>I Pad Apple</h1>                     
 <h2>Resolutionary</h2>          
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem 	
 ipsum dolor sit amet, consectetur adipiscing elit. 			
 Donec blandit enim libero, quis tincidunt arcu.</p>            
 </div>                                                    
 <img src="templates/<?php echo $this->template ?>/images/slide2.png" alt="Bolder Theme, for your corporate site" />      
 </div>			                                                               

 <div id="mac-book" class="panel none">         
 <div class="text">                                       
 <h1>Mac Book Pro</h1>                       
 <h2>with retina display</h2>              
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem 	
 ipsum dolor sit amet, consectetur adipiscing elit. 				
 Donec blandit enim libero, quis tincidunt arcu.</p>             
 </div>                                                    
 <img src="templates/<?php echo $this->template ?>/images/slide3.png" alt="Bolder Theme, for your corporate site" />              
 </div>                                                                 

 <div id="pod-touch" class="panel none">              
 <div class="text">                                
 <h1>i pod touch</h1>                         
 <h2>different color &#038; style</h2>        
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem 	
 ipsum dolor sit amet, consectetur adipiscing elit. 					
 Donec blandit enim libero, quis tincidunt arcu.</p>              
 </div>                       
 <img src="templates/<?php echo $this->template ?>/images/slide4.png" alt="Bolder Theme, for your corporate site" />				
 </div>                                                                 

 <div id="mac-apple" class="panel none">        
 <div class="text">                                   
 <h1>I MAC APPLE</h1>                     
 <h2>Professionnal computer</h2>        
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem 		
 ipsum dolor sit amet, consectetur adipiscing elit. 					
 Donec blandit enim libero, quis tincidunt arcu.</p>			
 </div>                    
 <img src="templates/<?php echo $this->template ?>/images/slide5.png" alt="Bolder Theme, for your corporate site" />					 
 </div>                              
 </div>              
 <div class="clear">
 </div>     
 </div>	
 </div>        

 <div id="nav-slider">			
 <div class="pagewidth">		
 <div class="inner">              
 <ul>                      
 <li class="current first"><a href="#i-phone">I Phone Apple </a><span>for your corporate site</span></li>				
 <li><a href="#elegant-theme">  I Pad Apple</a><span>different color &#038; style</span></li>				     
 <li><a href="#easy-customize"> Mac Book Pro</a><span>with retina display</span></li>				        
 <li><a href="#versatile-theme"> I Pod Touch</a><span>different color &#038; style</span></li>				
 <li><a href="#portfolio-blog">I Mac Apple </a><span>Professionnal computer</span></li>                    
 </ul>                
 </div>               
 <div class="clear"></div>			
 </div>     
 </div>	
 </div>	            

 <script type="text/javascript">	
 var $j = jQuery.noConflict();     
 jQuery(document).ready(function($){     
 $j('#nav-slider ul').arrowFade({		
 speed: 500,		
 timeout: 5000				
 });        });         
 </script>   
