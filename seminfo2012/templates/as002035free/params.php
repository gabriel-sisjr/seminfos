<?php 

/*******************************************************************************************/
/*
/*		Designed by 'AS Designing'
/*		Web: http://www.asdesigning.com
/*		Email: info@asdesigning.com
/*		License: ASDE Commercial
/*
/*******************************************************************************************/


$page_width 		= 1002;
$sidecolumn_width 	= 228;
$padding 			= 30;
$main_sepwidth 		= 30;
$footer_sidepadding = 30;

$slider_speed = 1000;
$slider_showtime = 4000;

$logo_img = $this->baseurl . '/templates/' . $this->template . '/images/companyname.png';

$leftcolumn = 0;
$leftcolumn += (bool) $this->countModules('position-40');
$leftcolumn += (bool) $this->countModules('position-41');

$main_width = $page_width;

if ($leftcolumn)
{
	$main_width = $page_width - $sidecolumn_width - $padding;
}

$main_componentwidth = $main_width;

?>


<style type="text/css">

#colmain
{
	width: <?php echo $main_width; ?>px;
}

#colmain #component
{
	width: <?php echo $main_componentwidth; ?>px;
}

</style>

