<?php 

/*******************************************************************************************/
/*
/*		Designed by 'AS Designing'
/*		Web: http://www.asdesigning.com
/*		Email: info@asdesigning.com
/*		License: GNU/GPL
/*
/*******************************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

/* The following line loads the MooTools JavaScript Library */
JHTML::_('behavior.framework', true);

/* The following line gets the application object for things like displaying the site name */
$app = JFactory::getApplication();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >

<head>
	<jdoc:include type="head" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/tmpl.content.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/tmpl.header.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/tmpl.sidebars.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/tmpl.footer.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/tmpl.general.css" type="text/css" />    

    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/slider/css/slider.css" type="text/css" 
    	media="screen" />
    
	<script type="text/javascript" src="templates/<?php echo $this->template ?>/slider/scripts/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="templates/<?php echo $this->template ?>/slider/scripts/jquery.slider.pack.js"></script>       
	<script type="text/javascript">

		$(window).load(function() 
		{
			$("<span class='btnbg_left'></span>").insertBefore("#topmenu .menu li a");							
			$("<span class='btnbg_right'></span>").insertAfter("#topmenu .menu li a");
			$("#topmenu .menu li li span").remove();
			
			$("<li class='bgtop'></li>").prependTo("#topmenu ul.menu ul");
			$("<li class='bgbottom'></li>").appendTo("#topmenu ul.menu ul");			
			
			$(".menu li").fadeIn(1);				
		
			$("<span class='btnbg_left'></span>").insertBefore("#colmain #component p.readmore a");							
			$("<span class='btnbg_right'></span>").insertAfter("#colmain #component p.readmore a");
			
		});    
    
    </script>
    
<?php 

include 'ie6warning.php';
include 'params.php';

?>
</head>

<body>

	<div class="wrapper">	
    <!-- HEADER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  -->
    <div id="header">
        <div class="row1">
	        <div class="content">
                <div class="row1col1">
                    <div id="companyname">
                        <a href="<?php echo $this->baseurl; ?>"> 
                            <img src="<?php echo $logo_img; ?>" alt="AS Templates"/>            
                        </a>
                    </div>
                </div>
                <div class="row1col2">
                    <div id="topmenu">
                        <jdoc:include type="modules" name="position-1"/>
                    </div>
				</div>
            </div>
		</div>
		<div class="clear"></div>

        <div class="row2">
	        <div class="content">
                <div id="sliderow">
                    <div id="slideshow">
                        <div class="slider-wrapper">
                            <div id="slider" class="nivoSlider">
                                <img src="templates/<?php echo  $this->template ?>/slider/images/slide1.png" />
                                <img src="templates/<?php echo  $this->template ?>/slider/images/slide2.png" />
                                <img src="templates/<?php echo  $this->template ?>/slider/images/slide3.png" />                           
                            </div>
                        </div>
                
                        <script type="text/javascript">
                        $(window).load(function() {
                            $('#slider').nivoSlider({
                            effect: 'fade',
                            animSpeed: <?php echo $slider_speed; ?>,
                            pauseTime: <?php echo $slider_showtime; ?>
                            });
                        });
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        
		<?php 
        if($this->countModules('position-2') || $this->countModules('position-0')): 
        ?>
        <div class="row4">
            <div class="content">
				<?php 
                if($this->countModules('position-0')): 
                ?>
            	<div class="row4col1">
                	<div class="bgleft"></div>
                    <div id="search"> 
                        <jdoc:include type="modules" name="position-0" />                        
                    </div>
                	<div class="bgright"></div>
                </div>
		        <?php endif; ?>
				<?php 
                if($this->countModules('position-2')): 
                ?>
            	<div class="row4col2">
                	<div class="bgleft"></div>
                    <div id="breadcrumb">
                    	<jdoc:include type="modules" name="position-2" />	
                    </div>
                	<div class="bgright"></div>
                </div>
		        <?php endif; ?>
            </div>
        </div>
        <div class="clear"></div>
        <?php endif; ?>

    </div>
    <!-- END OF HEADER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    
    <!-- CONTENT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="clear"></div>
    <div id="content">  
      
        <!-- COLUMN LEFT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <?php if($leftcolumn) : ?> 
        <div id="colleft">
            <?php if($this->countModules('position-40')): ?>
            <div class="row1">
                <jdoc:include type="modules" name="position-40" style="xhtml"/>
            </div>
            <?php endif; ?>
            <?php if($this->countModules('position-41')): ?>
            <div class="row2">
                <jdoc:include type="modules" name="position-41" style="xhtml"/>
            </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <!-- END OF COlUMN LEFT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                
        <!-- COLUMN MAIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <div id="colmain" >
        
            <jdoc:include type="message" />

            <div id="component">
               	<jdoc:include type="component"  />
            </div>
            <div class="clear"></div>
    
            <?php if($this->countModules('position-5')): ?>
            <div id="adsense">
            	<div class="innerborder">
                	<jdoc:include type="modules" name="position-5" style="xhtml"/>
                </div>
            </div>
            <?php else: ?>
            <div id="adsense">
            	<div class="innerborder">
					<!-- Your add here -->
                </div>
            </div>
            <div class="clear"></div>
            <?php endif; ?>

        </div>
        <!-- END OF COLUMN MAIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

    </div>
    <div class="clear"></div>

    <!-- FOOTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<div id="footer">
    	<div class="row3"> 
            <div class="content">
                <div class="row3col1">
                <!-- DO NOT REMOVE OR CHANGE THE CONTENT BELOW, THIS THEME MAY NOT WORK PROPERLY -->
                
                <div id="ascopy">
                <a href="http://www.astemplates.com/" target="_blank">
                    Designed by:&nbsp;&nbsp;AS DESIGNING
                </a>
                </div>
                
                <!-- DO NOT REMOVE OR CHANGE THE CONTENT ABOVE, THIS THEME MAY NOT WORK PROPERLY -->
                </div>
                <div class="row3col2">            
                    <div id="trademark">
                        Copyright &copy; <?php echo date('Y'); ?> <?php echo $app->getCfg('sitename'); ?>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <!-- END OF FOOTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    </div>
    
</body>
</html>

