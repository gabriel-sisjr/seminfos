<?php
/**
 * @author JoomlaShine.com Team
 * @copyright JoomlaShine.com
 * @link joomlashine.com
 * @package JSN ImageShow
 * @version $Id: sourceflickr.php 8606 2011-09-30 02:31:26Z trungnq $
 * @license GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die('Restricted access');
class TableSourceFlickr extends JTable
{
	var $external_source_id = null;
	var $external_source_profile_title = null;
	var $flickr_api_key = null;
	var $flickr_secret_key = null;
	var $flickr_username = null;
	var $flickr_caching = null;
	var $flickr_cache_expiration = null;
	var $flickr_image_size = null;

	function __construct(& $db) {
		parent::__construct('#__imageshow_external_source_flickr', 'external_source_id', $db);
	}
}
?>