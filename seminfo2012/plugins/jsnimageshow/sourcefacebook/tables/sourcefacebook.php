<?php
/**
 * @author JoomlaShine.com Team
 * @copyright JoomlaShine.com
 * @link joomlashine.com
 * @package JSN ImageShow
 * @version $Id: sourcefacebook.php 15502 2012-08-25 11:10:45Z haonv $
 * @license GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die('Restricted access');
class TableSourceFacebook extends JTable
{
	var $external_source_id = null;
	var $external_source_profile_title = null;
	var $facebook_access_token = null;
	var $facebook_app_id = null;
	var $facebook_app_secret = null;
	var $facebook_user_id = null;
	var $facebook_thumbnail_size = null;
	var $facebook_image_size = null;

	function __construct(& $db) {
		parent::__construct('#__imageshow_external_source_facebook', 'external_source_id', $db);
	}
	function store($updateNulls = false)
	{
		$query = 'SELECT * FROM #__imageshow_external_source_facebook WHERE external_source_id ='.(int)$this->external_source_id;
		$this->_db->setQuery($query);
		$current = $this->_db->loadObject();
		$updateThumbnailSize = false;
		$updateImageSize 	 = false;

		if ($current)
		{
			if ($this->facebook_thumbnail_size && $this->facebook_thumbnail_size != $current->facebook_thumbnail_size) {
				$updateThumbnailSize = $this->facebook_thumbnail_size;
			}

			if ($this->facebook_image_size && $this->facebook_image_size != $current->facebook_image_size) {
				$updateImageSize = $this->facebook_image_size;
			}
		}

		if (parent::store($updateNulls = false))
		{
			if (isset($updateImageSize)) {
				$this->updateImageSize($this->external_source_id, $updateImageSize);
			}

			if (isset($updateThumbnailSize)) {
				$this->updateThumbnailSize($this->external_source_id, $updateThumbnailSize);
			}
		} else {
			return false;
		}
		return true;
	}
	function getFacebookImageUrl($photoID,$size){
		$imgSrc = '';
		$graphUrl 	= 'https://graph.facebook.com/'.$photoID.'?access_token='.$this->facebook_access_token;
		$objJSNHTTPRequest = JSNISFactory::getObj('classes.jsn_is_httprequest', null, $graphUrl);
		$response = $objJSNHTTPRequest->DownloadToString();
		$image = json_decode($response,true);

		if (isset($image->error)||$response=="false"){
			$imgSrc="error";
		}else{
			$imgArr = $image['images'];
			$countImgArr = count($imgArr)-1;
			for($i=0;$i<$countImgArr;$i++){
				$imgSrc = $imgArr[$i]['source'];
				if($imgArr[$i]['width']<=$size)
					break;
			}
		}
		return $imgSrc;

	}
	function updateImageSize($externalSourceId, $updateImageSize = 960)
	{

		if (!$updateImageSize || !$externalSourceId) return false;

		$objJSNShowlist = JSNISFactory::getObj('classes.jsn_is_showlist');
		$objJSNImages	= JSNISFactory::getObj('classes.jsn_is_images');
		$showlists 		= $objJSNShowlist->getListShowlistBySource($externalSourceId, 'facebook');
		$db = JFactory::getDBO();
		foreach ($showlists as $showlist)
		{
			$images = $objJSNImages->getImagesByShowlistID($showlist->showlist_id);

			if ($images)
			{
				foreach ($images as $image)
				{
					$imageBig = $this->getFacebookImageUrl($image->image_extid,$this->facebook_image_size);
					if($imageBig!="error"&&$imageBig!=''){
						$query = 'UPDATE #__imageshow_images
								  SET image_big = '.$this->_db->quote($imageBig).'
								  WHERE showlist_id ='. (int)$showlist->showlist_id .'
								  AND image_id = '.$this->_db->quote($image->image_id);
						$db->setQuery($query);
						$db->query();
					}
				}
			}
		}
	}

	function updateThumbnailSize($externalSourceId, $updateThumbnailSize = 130)
	{
		if (!$updateThumbnailSize || !$externalSourceId) return false;

		$objJSNShowlist = JSNISFactory::getObj('classes.jsn_is_showlist');
		$objJSNImages	= JSNISFactory::getObj('classes.jsn_is_images');
		$showlists 		= $objJSNShowlist->getListShowlistBySource($externalSourceId, 'facebook');
		$db = JFactory::getDBO();
		foreach ($showlists as $showlist)
		{
			$images = $objJSNImages->getImagesByShowlistID($showlist->showlist_id);

			if ($images)
			{
				foreach ($images as $image)
				{
					$imageSmall = $this->getFacebookImageUrl($image->image_extid,$this->facebook_thumbnail_size);
					if($imageSmall!="error"&&$imageSmall!=""){
						$query = 'UPDATE #__imageshow_images
								  SET image_small = '.$this->_db->quote($imageSmall).'
								  WHERE showlist_id ='. (int)$showlist->showlist_id .'
								  AND image_id = '.$this->_db->quote($image->image_id).'
								  LIMIT 1';
						$db->setQuery($query);
						$db->query();
					}
				}
			}
		}
	}
}
?>