Easy Google Analytics

Version: 2.0
Copyright: Michael A. Gilkes
License: GNU/GPL v2


Minimum Requirements:
 > Joomla 1.5, 1.7 & 2.5
 > PHP 5.2+
 

Description:
This is a flexible, simple-to-use system plugin that is used to add Google Analytics
Tracking (asynchronous) Code to a Joomla site. It adds the Google tracking code to 
the head tag of each page of the website without having to edit any template code. 
This plugin uses the asynchronous tracking code for tracking a single domain, one 
domain with multiple subdomains, or multiple TLD domains.

Note, though, that the tracking code will be added at the end of the list of script
tags that Joomla generates, but since the Joomla generated tags usually come before
the template scripts, there is no guarantee that the tracking code will be the last 
script before the </head> tag.

Purchase:
This plugin is available for free.

Main features:
 > Automatiaclly places Google Analytics tracking code in head tag
 > Specify the Profile ID for your Analytics Account in the Plugin Manager
 > Choose what you are tracking: Single domain, Multiple Subdomains, or Multiple TLD
 > Automatic (or manual) entry of domain name for Multiple subdomain setting
 > Option to exclude Analytics in Administration backend


Changes:
See changelog.txt

Known Issues:
The tracking code may not be the last script tag before the &lt;/head&gt; tag, 
because of when the template script tags are added. This may cause a performance 
issue in some browsers, as specified by Google Help: 
http://www.google.com/support/googleanalytics/bin/answer.py?answer=174090



Installation:
This plugin is designed for Joomla 1.5, 1.7 and 2.5. To install go to the Extension Manager 
page of Joomla Administrator and upload the package file. Then go to the Plugin Manager 
page and activate the plugin.


Usage:
To use this plugin on the joomla site, first enable the plugin and configure the 
default parameters in the Plugin Manager. To control which types of users have access to 
this plugin, set the Access Level in the plugin.


Parameters:
Name       -      Default Value      -       Description
==========   =======================   ======================
Profile ID - UA-XXXXX-X - This is the profile ID of the Google Analytics account.
Tracking - Single Domain - This specifies what you are actually tracking. There are three options: Single Domain, One Domain with Multiple Subdomains, or Multiple TLD Domains
Hostname -  - This is the second-level domain and top-level domain that will be typically specified when you select the "One Domain with Multiple Subdomains" option. The plugin automatically fills this out for you.
Exclude Administration - Yes - If this option is set to Yes, Google Analytics will track access to the Administration pages.
