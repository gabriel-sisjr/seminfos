<?php
/**
* @version		2.0
* @author		Michael A. Gilkes (jaido7@yahoo.com)
* @copyright	Michael Albert Gilkes
* @license		GNU/GPLv2
*/

/*

Easy Google Analytics for Joomla!
Copyright (C) 2011-2012  Michael Albert Gilkes

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

// no direct access
defined('_JEXEC') or die('Restricted access');

//imports
jimport('joomla.plugin.plugin'); //needed for MVC implementation

class plgSystemEasyGoogleAnalytics extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @access      public
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.6
	 */
	public function __construct(&$subject, $config)
	{
		// Note:
		// $config should contain the params
		parent::__construct($subject, $config);
	}
	
	/**
	 * @param	none
	 *
	 * @return	none
	 * @since	1.6
	 */
	public function onBeforeCompileHead()
	{
		// check whether plugin has been unpublished
		if ($this->params->get('enabled', 1))
		{
			if ($this->params->get('ega_admin') == true)
			{
				//get application instance
				$app = JFactory::getApplication();
				
				//check to see if we are in the front-end or back-end
				if($app->isAdmin())
				{
					return; //leave since we are in the back-end. bye.
				}
			}
			
			//get the parameters
			$profileID = $this->params->get('ega_profileid');
			$tracking = $this->params->get('ega_tracking');
			$hostname = $this->params->get('ega_hostname');
			
			//get the completed asynchronous tracking code
			$script = $this->customTrackingCode($profileID, $tracking, $hostname);
			
			//add the script references
			//**********************************			
			//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
			$document =& JFactory::getDocument();
			
			//add the javascript to the head of the html document
			$document->addScriptDeclaration($script);
		}
	}
	
	/**
	* method that composes the tracking code for a single domain
	*/
	protected function customTrackingCode($profileID, $tracking, $hostname)
	{
		$script = "\n\n".'/*===  EASY GOOGLE ANALYTICS : START TRACKING CODE  ===*/'."\n";
		$script.= "\n\t"."var _gaq = _gaq || [];";
		$script.= "\n\t"."_gaq.push(['_setAccount', '".$profileID."']);";
		if ($tracking == 'subdomains')
		{
			$script.= "\n\t"."_gaq.push(['_setDomainName', '.".$hostname."']);";
		}
		elseif ($tracking == 'tld')
		{
			$script.= "\n\t"."_gaq.push(['_setDomainName', 'none']);";
			$script.= "\n\t"."_gaq.push(['_setAllowLinker', true]);";
		}
		$script.= "\n\t"."_gaq.push(['_trackPageview']);"."\n";
		$script.= "\n\t"."(function() {";
		$script.= "\n\t\t"."var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;";
		$script.= "\n\t\t"."ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';";
		$script.= "\n\t\t"."var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);";
		$script.= "\n\t"."})();"."\n";
		$script.= "\n".'/*===  EASY GOOGLE ANALYTICS : END TRACKING CODE  ===*/'."\n";
		
		return $script;
	}
} //end of class