<?php
/**
* @package   yoo_steam
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
die('Restricted access');

?>

Changelog
------------

1.0.4
# Fixed iPad crash issue

1.0.3
# Fixed .line-icon :first-child background display

1.0.2
^ Updated menu icons and titles according to Warp 6.2

1.0.1
# Fixed Mainmenu for Safari
# Fixed Pagination overrides

1.0.0
+ Initial Release



* -> Security Fix
# -> Bug Fix
$ -> Language fix or change
+ -> Addition
^ -> Change
- -> Removed
! -> Note