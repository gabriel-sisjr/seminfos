<?php die();?>
Contact Info:
================================================================================
Developer: Michael A. Gilkes
Website: https://valorapps.com
Blog: http://michaelgilkes.info
email: michael@valorapps.com or jaido7@yahoo.com

Copyright Info:
================================================================================
Copyright: Michael A. Gilkes
License: GNU/GPL v2

Notes:
================================================================================
> Additions are represented by plus sign (+)
> Removals are represented by minus sign (-)
> Changes are represented by tilde sign (~)
> Notices are represented by hash sign (#)
> Issues are represented by exclamation sign (!)

Easy Google Analytics v2.5
================================================================================
+ Added feature to track Outbound Links via Google Event Tracking [2013-07-08]
- Removed all reference assignments (=&) [2013-07-08]
+ Added option to use Universal Analytics Tracking Code [2013-07-02]

Easy Google Analytics v2.2
================================================================================
+ Added documentation link and key in manifest file [2013-01-29]
~ Changed extension upgrade method from 'new' to 'upgrade' [2013-01-29]
~ Changed custom INI keys for Yes, No and None to Joomla Defaults [2013-01-29]
~ Changed the format, file type, and file name for the change log file [2013-01-29]
+ Added script file for making extension stay as single installer [2013-01-29]

Easy Google Analytics v2.0
================================================================================
+ Compatible with Joomla 2.5 [2012-02-24]
~ Changed triggering in onAfterDispatch() for Joomla 1.5 to onBeforeCompileHead() [2012-02-24]
+ Added Language File capability [2012-02-24]
~ Combined all packages into a single installation package [2012-02-24]

Easy Google Analytics v1.0
================================================================================
~ Tested for Joomla 1.7 [2011-09-12]
+ Added option to exclude tracking of Administration pages [2011-09-12]

Easy Google Analytics v0.3
================================================================================
+ Added automatic detection of hostname (sld dot tld) [2011-06-14]
+ Added option to track Single domain, Multiple Subdomains, ot Multiple TLD [2011-06-14]

Easy Google Analytics v0.1
================================================================================
+ Plugin creation completed and tested. [2011-05-27]
